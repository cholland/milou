/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2013  Vishesh Handa <me@vhanda.in>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "videoplugin.h"

#include <QVBoxLayout>

#include <Phonon/VideoPlayer>
#include <Phonon/SeekSlider>

VideoPlugin::VideoPlugin(QObject* parent, const QVariantList& ): PreviewPlugin(parent)
{

}

void VideoPlugin::generatePreview()
{
    QWidget* widget = new QWidget();
    QVBoxLayout* layout = new QVBoxLayout(widget);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    Phonon::VideoPlayer* player = new Phonon::VideoPlayer(widget);
    player->load(Phonon::MediaSource(url()));

    Phonon::SeekSlider* seekSlider = new Phonon::SeekSlider(widget);
    seekSlider->setMediaObject(player->mediaObject());
    seekSlider->setIconVisible(false);

    layout->addWidget(player);
    layout->addWidget(seekSlider);

    player->play();
    player->resize(300, 300);

    emit previewGenerated(widget);
}

MILOU_EXPORT_PREVIEW(VideoPlugin, "milouvideoplugin", "milou")
